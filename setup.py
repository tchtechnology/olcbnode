from setuptools import find_packages, setup

setup(
    name='OLCBNode',
    version='0.1',
    description='OpenLCB Node Server',
    author='Timothy Hatch',
    author_email='wb0wuq@gmail.com',
    url='http://www.tch-technology.com/',
    license='Apache-2.0',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    entry_points={
        'console_scripts': [
            'olcbnode=olcbnode.main:main',
        ],
    },
)
