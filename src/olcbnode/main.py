from __future__ import print_function, unicode_literals
from . import server
import argparse
import logging
import multiprocessing


log = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', '-p', default=12817, type=int,
                        help='Listen on specified TCP port')
    parser.add_argument('--num-threads', '-T', metavar='COUNT',
                        default=multiprocessing.cpu_count(),
                        help='Number of worker threads')
    parser.add_argument('--verbose', '-v', action='count', default=0,
                        help='Display detailed output, repeat to increase')
    return parser.parse_args()


def main():
    args = parse_args()

    if args.verbose:
        level = logging.INFO
        if args.verbose >= 2:
            level = logging.DEBUG
        if args.verbose >= 3:
            server.Worker.DEBUG_MESSAGES = True
        logging.basicConfig(level=level)

    s = server.Server(args.num_threads, args.port)
    try:
        s.serve()
    except KeyboardInterrupt:
        print('')
        s.stop()
