from __future__ import unicode_literals
import itertools
import logging
import socket
import threading
try:
    import queue
except ImportError:
    import Queue as queue


log = logging.getLogger(__name__)


class Server(object):

    DEFAULT_ADDRESS = ''
    DEFAULT_PORT = 12817

    def __init__(self, num_workers=1, port=None, address=None):
        if address is None:
            address = self.DEFAULT_ADDRESS
        if port is None:
            port = self.DEFAULT_PORT
        self.address = address
        self.port = port
        self.num_workers = num_workers
        self.socket = None
        self.running = True
        self.connq = queue.Queue()
        self.workers = []
        log.info('Starting server with {} workers'.format(self.num_workers))
        while len(self.workers) < self.num_workers:
            w = Worker(self.connq)
            self.workers.append(w)
            w.start()

    def serve(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.address, self.port))
        self.socket.listen(10)
        while self.running:
            self.connq.put(self.socket.accept())

    def stop(self):
        log.info('Shutting down server')
        self.running = False
        for worker in self.workers:
            worker.stop()
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()
        self.socket = None


class Worker(threading.Thread):

    BUFSIZE = 4096
    DEBUG_MESSAGES = False

    _serial = itertools.count(1)

    def __init__(self, connq):
        super(Worker, self).__init__()
        self.name = '{}-{}'.format(self.__class__.__name__, next(self._serial))
        self.connq = connq
        self.running = True

    def stop(self):
        log.debug('{} shutting down'.format(self.name))
        self.running = False
        self.connq.put((None, None))

    def run(self):
        while self.running:
            conn, addr = self.connq.get()
            if conn is None:
                break
            log.debug('Connection from {}'.format(addr))
            buf = bytearray()
            while True:
                d = conn.recv(self.BUFSIZE)
                if not d:
                    break
                buf += d
                if len(d) < self.BUFSIZE:
                    self.handle_message(conn, buf)
                    buf = bytearray()
            log.debug('Client {} disconnected'.format(addr))
            conn.close()

    def handle_message(self, conn, buf):
        log.debug('Received {} bytes'.format(len(buf)))
        if self.DEBUG_MESSAGES:
            log.debug('Received message: {!r}'.format(bytes(buf)))
